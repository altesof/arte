<?php
/* Smarty version 3.1.39, created on 2022-02-22 21:40:56
  from '/var/www/html/arte/themes/classic/templates/_partials/helpers.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6215586856ffa7_01034701',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4ea9b4e1c08e4b97c3370efb8c43d7e220286f85' => 
    array (
      0 => '/var/www/html/arte/themes/classic/templates/_partials/helpers.tpl',
      1 => 1643096356,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6215586856ffa7_01034701 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'renderLogo' => 
  array (
    'compiled_filepath' => '/var/www/html/arte/var/cache/prod/smarty/compile/4e/a9/b4/4ea9b4e1c08e4b97c3370efb8c43d7e220286f85_2.file.helpers.tpl.php',
    'uid' => '4ea9b4e1c08e4b97c3370efb8c43d7e220286f85',
    'call_name' => 'smarty_template_function_renderLogo_6470851326215586855dfa2_08658991',
  ),
));
?> 

<?php }
/* smarty_template_function_renderLogo_6470851326215586855dfa2_08658991 */
if (!function_exists('smarty_template_function_renderLogo_6470851326215586855dfa2_08658991')) {
function smarty_template_function_renderLogo_6470851326215586855dfa2_08658991(Smarty_Internal_Template $_smarty_tpl,$params) {
foreach ($params as $key => $value) {
$_smarty_tpl->tpl_vars[$key] = new Smarty_Variable($value, $_smarty_tpl->isRenderingCache);
}
?>

  <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['index'], ENT_QUOTES, 'UTF-8');?>
">
    <img
      class="logo img-fluid"
      src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo_details']['src'], ENT_QUOTES, 'UTF-8');?>
"
      alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
"
      width="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo_details']['width'], ENT_QUOTES, 'UTF-8');?>
"
      height="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo_details']['height'], ENT_QUOTES, 'UTF-8');?>
">
  </a>
<?php
}}
/*/ smarty_template_function_renderLogo_6470851326215586855dfa2_08658991 */
}
